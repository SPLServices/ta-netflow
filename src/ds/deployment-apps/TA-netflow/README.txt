Copyright (C) 2015-2016 NetFlow Logic Corporation. All Rights Reserved.

App:                Technology Add-On for NetFlow
Supported products: Netflow Analytics for Splunk (version 3.5 or above) and Enterprise Security (version 3.0 or above)
Last Modified:      2016-2-05
Splunk Version:     6.x
Author:             NetFlow Logic

This TA relies on NetFlow Integrator software.
To download a free trial of NetFlow Integrator, please visit
https://www.netflowlogic.com/downloads/.

This TA creates a UDP data input on port 10514 to receive syslog messages and puts the events in index=flowintegrator and sets sourcetype to sourcetype=flowintegrator.
It provides CIM compliant field names, eventtypes and tags for NetFlow Integrator data.
The TA can also be used to generate sample events for testing purposes, it contains samples of netflow data and config files for the event generator.

##### Installation #####

1. Download TA-netflow from  https://apps.splunk.com/app/1838/ and install it on the indexers.

##### Usage with Enterprise Security

   Input requirements: If they are applicable, then the following rules should be enabled in NetFlow Integrator :

        10020/20020 ( Top Policy Violators for Cisco ASA )
        10032/20032 ( Hosts with Most Policy Violations for Palo Alto Networks )
        10050/20050 ( Botnet Command and Control Traffic Monitor )
        10052/20052 ( Peer by Reputation Monitor )
        10053/20053 ( Threat Feeds Traffic Monitor )
        10067/20067 ( Top Traffic Monitor)

   By default the netflow data is routed by default to index=flowintegrator.
   The admin role must be configured to look also at the index where netflow data is stored.
   This can be achieved by changing the admin role with adding the additional index to the list of “Indexes searched by default”.

   Warning : When adding indexes to the default search indexes do not include any summary indexes, as this can cause a search and summary index loop.

##### How to enable the netflow event generator for testing purposes

    This functionality relies on the "The Splunk Event Generator" software available from https://github.com/coccyx/eventgen

    Install the eventgen app and after that:
    1) Create directory $SPLUNK_ROOT/etc/apps/TA-netflow/local/ if it doesn't exist
    2) Copy eventgen.conf from /default to /local folder and change the line:
        disabled = true
      to
        disabled = false
    3) Restart Splunk
    
    When the generated samples are no longer needed they can be permanently deleted from the flowintegrator index with the following command:
    
    ./splunk clean eventdata -index flowintegrator
    
    Warning : the command above permanently deletes all the current data in the flowintegrator index.
    
##### Documentation #####

To get the most up-to-date information on how to install, configure, and use the App,
download "NetFlow Analytics for Splunk User Manual" pdf document 
by visiting https://www.netflowlogic.com/resources/documentation/

###### Get Help ######

Have questions or need assistance? We are here to help! Please visit
https://www.netflowlogic.com/connect/support/
